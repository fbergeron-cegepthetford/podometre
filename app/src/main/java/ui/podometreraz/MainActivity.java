package ui.podometreraz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText etObjectifPerso;
    Button btnGo;
    Context cx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cx = this;

        initialiserVue();
    }

    private void initialiserVue() {
        setContentView(R.layout.activity_main);

        etObjectifPerso = findViewById(R.id.etObjectifPerso);

        btnGo = findViewById(R.id.btnGo);
        btnGo.setOnClickListener(view -> {
            try {
                int objectifPersonnel = Integer.parseInt(etObjectifPerso.getText().toString());
                Intent intention = new Intent(cx, ActivitePrincipale.class);

                intention.putExtra("objectifPersonnel", objectifPersonnel);
                startActivity(intention);

            } catch (Exception e){
                Toast.makeText(this, "Veuillez entrer une valeur valide", Toast.LENGTH_LONG).show();
            }
        });
    }
}