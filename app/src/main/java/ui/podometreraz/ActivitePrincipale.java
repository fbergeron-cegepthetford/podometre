package ui.podometreraz;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;

import affaire.podometreraz.Podometre;

public class ActivitePrincipale extends AppCompatActivity
        implements Observer, View.OnClickListener, DialogInterface.OnClickListener {
    private Podometre podometre;
    private TextView tvValeur, tvDate, tvObjectifPerso, tvTimer;
    AlertDialog dialogue;
    boolean estEnFonction = false;
    long delai = 90001;
    boolean etatBtFin;
    int objectifPersonnel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.initialiserAffaire();
        this.initialiserVue();
        recupererObjectifPerso();
        this.dateEtHeure();
        this.initialiserDialogue();
        lancerTimer(delai);
    }

    private void recupererObjectifPerso() {
        objectifPersonnel = this.getIntent().getIntExtra("objectifPersonnel", 0);
        try {
            tvObjectifPerso.setText(getString(R.string.objectif_personnel_d, objectifPersonnel));
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            this.finish();
        }
    }

    private void initialiserAffaire() {
        podometre = new Podometre();
        podometre.addObserver(this);
    }

    private void initialiserVue() {
        this.setContentView(R.layout.activite_principale);
        tvValeur = this.findViewById(R.id.tvValeur);
        tvDate = this.findViewById(R.id.tvDate);
        tvObjectifPerso = findViewById(R.id.tvObjectifPerso);
        Button btMoins = this.findViewById(R.id.btMoins);
        btMoins.setOnClickListener(this);
        Button btPlus = this.findViewById(R.id.btPlus);
        btPlus.setOnClickListener(this);
        Button btFin = this.findViewById(R.id.btFin);
        btFin.setOnClickListener(this);
        Button btnClick = this.findViewById(R.id.btnClick);
        btnClick.setOnClickListener(view -> {
            Intent intention = new Intent(this, ActiviteSecondaire.class);
            startActivity(intention);
        });
        etatBtFin = false;
        tvTimer = findViewById(R.id.tvTimer);
        tvObjectifPerso = findViewById(R.id.tvObjectifPerso);
        lancerTimer(delai);
        reafficher();
    }

    private void initialiserDialogue() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(this.getResources().getText(R.string.question_dialogue));
        builder.setNegativeButton(this.getResources().getText(R.string.reponse_non),
                this);
        builder.setPositiveButton(this.getResources().getText(R.string.reponse_oui),
                this);
        dialogue = builder.create();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.menu_activite_principale, menu);
        return true;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean retour = super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.action_raz:
                dialogue.show();
                break;
            case R.id.action_Inc:
                podometre.incrementer();
                break;
            case R.id.action_Dec:
                podometre.decrementer();
                break;
            case R.id.action_nouvel_objectif:
                Intent intention = new Intent(this, MainActivity.class);
                startActivity(intention);
                break;
        }

        return retour;
    }

    @SuppressLint({"NonConstantResourceId", "SetTextI18n"})
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btMoins:
                podometre.decrementer();
                int valeur = Integer.parseInt(tvValeur.getText().toString());

                if(valeur < 30){
                    tvValeur.setBackgroundColor(Color.rgb(255,0,0));
                } else if(valeur < 60){
                    tvValeur.setBackgroundColor(Color.rgb(205,127,50));
                } else if (valeur<100){
                    tvValeur.setBackgroundColor(Color.rgb(192,192,192));
                } else {
                    tvValeur.setBackgroundColor(Color.rgb(255,215,0));
                }
                etatBtFin = false;
                break;
            case R.id.btPlus:
                podometre.incrementer();
                int valeur2 = Integer.parseInt(tvValeur.getText().toString());

                if(valeur2 < 30){
                    tvValeur.setBackgroundColor(Color.rgb(255,0,0));
                } else if(valeur2 < 60){
                    tvValeur.setBackgroundColor(Color.rgb(205,127,50));
                } else if (valeur2<100){
                    tvValeur.setBackgroundColor(Color.rgb(192,192,192));
                } else {
                    tvValeur.setBackgroundColor(Color.rgb(255, 215, 0));
                }
                etatBtFin = false;
                break;
            case R.id.btFin:
                if (!etatBtFin)
                    tvValeur.setText("Vous avez cliqué " + tvValeur.getText() + " Fois");
                etatBtFin = true;
                Intent intent = new Intent(ActivitePrincipale.this, UserFinal.class);
                startActivity(intent);
                System.exit(0);
                break;
        }
    }

    @Override
    public void update(Observable observable, Object o) {
        if (observable == podometre) {
            this.reafficher();
            if (objectifPersonnel == podometre.getValeur()) {
                MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.yahaha_korok);
                mediaPlayer.start();
            }
        }
    }

    private void reafficher() {
        tvValeur.setText(getString(R.string.valeur, podometre.getValeur()));
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if (dialogInterface == dialogue)
            switch (i) {
                case DialogInterface.BUTTON_NEGATIVE:
                    // Ne rien faire...  L'utilisateur a répondu "Non"...

                    break;
                case DialogInterface.BUTTON_POSITIVE:
                    this.reinitialiser();
            }
    }

    private void dateEtHeure() {
        Calendar c = Calendar.getInstance();
        System.out.println(c.getTime());

        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formatDate = df.format(c.getTime());

        Toast.makeText(this, formatDate, Toast.LENGTH_LONG).show();

        tvDate.setText(formatDate);
    }

    private void reinitialiser() {
        podometre.raz();
        tvValeur.setBackgroundColor(Color.rgb(255, 0, 0));
    }

    public void lancerTimer(long delai) {
        if (!estEnFonction) {
            estEnFonction = true;
            CountDownTimer countDownTimer = new CountDownTimer(delai, 1000) {
                @SuppressLint("DefaultLocale")
                public void onTick(long millisUntilFinished) {
                    long minutes = (millisUntilFinished / (60 * 1000)) % 60;
                    long secondes = (millisUntilFinished / 1000) % 60;
                    if ((Math.round(Math.floor(millisUntilFinished)) / 1000) % 2 == 0)
                        tvObjectifPerso.setBackgroundColor(getResources().getColor(R.color.teal_200));
                    else
                        tvObjectifPerso.setBackgroundColor(getResources().getColor(R.color.purple_200));
                    tvTimer.setText(String.format("%d : %d", minutes, secondes));
                }

                public void onFinish() {
                    estEnFonction = false;
                    lancerTimer(delai);
                }
            };
            countDownTimer.start();
        }
    }
}