package ui.podometreraz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class UserFinal extends AppCompatActivity implements View.OnClickListener {

    Button btnReturn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialiseView();
    }

    private void initialiseView() {
        setContentView(R.layout.activity_user_final);

        btnReturn = this.findViewById(R.id.btnReturn);
        btnReturn.setOnClickListener(view ->{
            Intent intention = new Intent(this, MainActivity.class);
            startActivity(intention);
        });
    }

    @Override
    public void onClick(View view) {

    }
}