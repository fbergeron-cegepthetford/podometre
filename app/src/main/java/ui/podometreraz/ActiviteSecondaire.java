package ui.podometreraz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

public class ActiviteSecondaire extends AppCompatActivity {

    VideoView vvRick;
    Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialiserVue();

    }
    private void initialiserVue() {
        setContentView(R.layout.activity_activite_secondaire);

        vvRick = this.findViewById(R.id.vvRick);
        String videoPath = "android.resource://" + getPackageName() + "/" + R.raw.rick;
        Uri uri = Uri.parse(videoPath);
        vvRick.setVideoURI(uri);

        MediaController mediaController = new MediaController(this);
        vvRick.setMediaController(mediaController);
        mediaController.setAnchorView(vvRick);

        btnBack = this.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(view ->{
            Intent intention = new Intent(this, MainActivity.class);
            startActivity(intention);
        });
    }
}