package affaire.podometreraz;

import java.util.Observable;

public class Podometre extends Observable {
    private int valeur;

    public Podometre() {
        valeur = 0;
    }

    public int getValeur() {
        return valeur;
    }

    public void incrementer() {
        valeur++;
        this.avertirObservateurs();
    }


    public void decrementer() {
        valeur--;
        this.avertirObservateurs();
    }

    public void raz() {
        valeur = 0;
        this.avertirObservateurs();
    }

    private void avertirObservateurs() {
        this.setChanged();
        this.notifyObservers();
    }
}

